GOCMD=go
GOBUILD=$(GOCMD) build
lambda-deploy: clean build-lambda pack post-pack deploy-lambda
lambda-pack: clean build-lambda pack post-pack
build-lambda:
	CC="musl-gcc" $(GOBUILD) --ldflags '-linkmode external -extldflags "-static"' ./main.go
pack:
	zip function.zip main
post-pack:
	rm -f main
clean:
	rm -f function.zip main
deploy-lambda:
	sam build && sam deploy
build-WordOfDayBot:
	CC="musl-gcc" $(GOBUILD) --ldflags '-linkmode external -extldflags "-static"' ./main.go
	cp ./main $(ARTIFACTS_DIR)