module github.com/kevinsj/word-of-day-bot

go 1.15

require (
	github.com/aws/aws-lambda-go v1.22.0
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/joho/godotenv v1.3.0
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b
)
