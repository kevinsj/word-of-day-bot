package main

import (
	"log"
	"os"
	"strconv"
	"sync"

	"github.com/aws/aws-lambda-go/lambda"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/joho/godotenv"
	"github.com/kevinsj/word-of-day-bot/src/wodbot"
)

//HandleRequest AWS Lambda Request Handler
func HandleRequest() {
	if _, present := os.LookupEnv("_HANDLER"); !present {
		err := godotenv.Load()
		if err != nil {
			log.Panic(err)
		}
	}

	err := godotenv.Load()
	feeds := wodbot.EnabledFeeds
	msgs := wodbot.CommonParser(feeds)
	botToken := os.Getenv("BOT_TOKEN")
	bot, err := tgbotapi.NewBotAPI(botToken)
	if err != nil {
		log.Panic(err)
	}
	channelID, _ := strconv.ParseInt(os.Getenv("TG_CHANNEL_ID"), 10, 64)
	// bot.Debug = true
	var wg sync.WaitGroup
	for _, tgMsg := range msgs {
		msg := tgbotapi.NewMessage(channelID, tgMsg)
		wg.Add(1)
		go func(msg *tgbotapi.MessageConfig, wg *sync.WaitGroup) {
			msg.ParseMode = "markdown"
			bot.Send(msg)
			defer wg.Done()
		}(&msg, &wg)
	}

	wg.Wait()
}

func main() {
	lambda.Start(HandleRequest)
}
