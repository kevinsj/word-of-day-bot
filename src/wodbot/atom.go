package wodbot

//Feed struct for RSS
type Feed struct {
	Entries []Entry `xml:"entry"`
}

//Entry struct for each Entry in the Feed
type Entry struct {
	ID      string `xml:"id"`
	Title   string `xml:"title"`
	Updated string `xml:"updated"`
	Summary string `xml:"summary"`
}
