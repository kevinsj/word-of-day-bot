package wodbot

import (
	"encoding/xml"
	"fmt"
	"log"
	"net/http"

	"golang.org/x/net/html/charset"
)

// CommonParser is ....
func CommonParser(feeds []*Source) []string {
	var msgs []string
	for _, feed := range feeds {
		resp, err := http.Get(feed.URL)
		if err != nil {
			fmt.Printf("Error GET: %v\n", err)
			log.Panic(err)
		}
		defer resp.Body.Close()
		log.Println("Processing.. " + feed.Name)
		decoder := xml.NewDecoder(resp.Body)
		// This is needed as some of the feed is not in UTF-8
		decoder.CharsetReader = charset.NewReaderLabel
		var tgMsg string
		if feed.IsAtom {
			rss := Feed{}
			err = decoder.Decode(&rss)
			allEntries := rss.Entries
			tgMsg = feed.Parse(&allEntries[len(allEntries)-1])
		} else {
			rss := Rss{}
			err = decoder.Decode(&rss)
			firstItem := &rss.Channel.Items[0]
			tgMsg = feed.Parse(firstItem)
		}

		if err != nil {
			fmt.Printf("Error Decode: %v\n", err)
			log.Panic(err)
		}
		msgs = append(msgs, tgMsg)
	}
	return msgs
}
