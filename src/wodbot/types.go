package wodbot

// SourceParseMethod is method used to parse the feed
type SourceParseMethod func(item interface{}) string

// Enclosure represents url,length and type for item
type Enclosure struct {
	URL    string `xml:"url,attr" json:"url,omitempty"`
	Length int64  `xml:"length,attr" json:"length,omitempty"`
	Type   string `xml:"type,attr" json:"type,omitempty"`
}

// Item represent each item in rss feed
type Item struct {
	Title         string    `xml:"title" json:"title,omitempty"`
	Link          string    `xml:"link" json:"link,omitempty"`
	Desc          string    `xml:"description" json:"desc,omitempty"`
	GUID          string    `xml:"guid" json:"guid,omitempty"`
	Enclosure     Enclosure `xml:"enclosure" json:"enclosure,omitempty"`
	PubDate       string    `xml:"pubDate" json:"pub_date,omitempty"`
	ItunesSummary string    `xml:"summary" json:"itunes_summary,omitempty"`
	// Optional
	ContentTitle string `xml:"contentTitle" json:"content_title,omitempty"`
}

// Channel is a channel in rss feed.
type Channel struct {
	Title string `xml:"title" json:"title,omitempty"`
	Link  string `xml:"link" json:"link,omitempty"`
	Desc  string `xml:"description" json:"desc,omitempty"`
	Items []Item `xml:"item" json:"items,omitempty"`
}

// Rss is a root in rss feed.
type Rss struct {
	Channel Channel `xml:"channel"`
}

// Source is ...
type Source struct {
	Name   string `json:"name,omitempty"`
	URL    string `json:"feed_url,omitempty"`
	Parse  SourceParseMethod
	IsAtom bool
}
