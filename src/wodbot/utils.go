package wodbot

import (
	"log"
	"regexp"
)

func merriamWebsterParser(item interface{}) string {
	wordOfToday := "Word of today is " + item.(*Item).Title + "\n"
	summaryOfUsage := "Summary of usage: \n" + item.(*Item).ItunesSummary
	tgMsg := wordOfToday + summaryOfUsage
	return tgMsg
}

func dictionaryDotComParser(item interface{}) string {
	wodImg := regexp.MustCompile(`(?m)src="(.*png)"`).FindStringSubmatch(item.(*Item).Desc)[1]
	wod := regexp.MustCompile(`(?m)(^\D+)-`).FindStringSubmatch(item.(*Item).ContentTitle)[1]
	tgMsg := `[` + wod + `]` + `(` + wodImg + `)`
	return tgMsg
}

func genericParser(item interface{}) string {
	wordOfToday := "Word of today is " + item.(*Item).Title + "\n"
	summaryOfUsage := "Explaination: \n" + item.(*Item).Desc
	tgMsg := wordOfToday + summaryOfUsage
	log.Println(tgMsg)
	return tgMsg
}

func atomParser(entry interface{}) string {
	wordOfToday := "Word of today is " + entry.(*Entry).Title + "\n"
	summaryOfUsage := "Explaination: \n" + entry.(*Entry).Summary
	tgMsg := wordOfToday + summaryOfUsage
	return tgMsg
}

var oxfordFeed = &Source{Name: "oxford-wotd", URL: "https://feeds.feedburner.com/OLD-WordOfTheDay?fmt=xml", Parse: atomParser, IsAtom: true}

var macmillamBuzzWord = &Source{Name: "macmillam-buzz-word", URL: "https://www.macmillandictionary.com/buzzword/rss.xml", Parse: atomParser, IsAtom: true}

var macmillamPotw = &Source{Name: "macmillam-pharse-of-the-week", URL: "https://www.macmillandictionary.com/potw/potwrss.xml", Parse: atomParser, IsAtom: true}

var englishClubIdiomOfDay = &Source{Name: "english-club-idiom", URL: "https://www.englishclub.com/ref/idiom-of-the-day.xml", Parse: genericParser}

// MerriamWebsterFeed is feed for merriam webster
var merriamWebsterFeed = &Source{Name: "merriam-webster", URL: "https://www.merriam-webster.com/wotd/feed/rss2", Parse: merriamWebsterParser}

// DictDotComFeed is feed for merriam webster
var dictDotComFeed = &Source{Name: "dictDotCom", URL: "https://www.dictionary.com/e/word-of-the-day/?feed=wotd-podcasts", Parse: dictionaryDotComParser}

// EnabledFeeds is currently enabled feeds
var EnabledFeeds = []*Source{merriamWebsterFeed, dictDotComFeed, englishClubIdiomOfDay, macmillamPotw}
