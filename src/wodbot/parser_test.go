package wodbot

import (
	"reflect"
	"testing"
)

func TestCommonParser(t *testing.T) {
	type args struct {
		feeds []*Source
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CommonParser(tt.args.feeds); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CommonParser() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseMerriamWebster(t *testing.T) {
	type args struct {
		item Item
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := merriamWebsterParser(tt.args.item); got != tt.want {
				t.Errorf("parseMerriamWebster() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseDictionaryDotCom(t *testing.T) {
	type args struct {
		item Item
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := dictionaryDotComParser(tt.args.item); got != tt.want {
				t.Errorf("parseDictionaryDotCom() = %v, want %v", got, tt.want)
			}
		})
	}
}
